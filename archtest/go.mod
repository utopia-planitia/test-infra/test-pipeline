module archtest

go 1.22.0

toolchain go1.23.4

require (
	github.com/containerd/platforms v0.2.1
	github.com/moby/buildkit v0.18.2
	github.com/opencontainers/image-spec v1.1.0
)

require (
	github.com/containerd/log v0.1.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/tonistiigi/go-archvariant v1.0.0 // indirect
	go.opentelemetry.io/otel v1.28.0 // indirect
	go.opentelemetry.io/otel/trace v1.28.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
