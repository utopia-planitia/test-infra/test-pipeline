package main

import (
	"fmt"

	quote "rsc.io/quote/v3"
)

func main() {
	fmt.Printf("%s\n", quote.HelloV3())
}
